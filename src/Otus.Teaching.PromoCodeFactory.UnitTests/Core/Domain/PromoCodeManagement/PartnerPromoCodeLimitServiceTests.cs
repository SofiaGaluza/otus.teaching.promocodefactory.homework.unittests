﻿using AutoFixture.AutoMoq;
using AutoFixture;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Linq;
using Xunit;
using FluentAssertions;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Core.Domain.PromoCodeManagement
{
	public class PartnerPromoCodeLimitServiceTests
	{
		private readonly IPartnerPromoCodeLimitService _partnersPromoCodeLimitService;
		private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
		public PartnerPromoCodeLimitServiceTests()
		{
			var fixture = new Fixture().Customize(new AutoMoqCustomization());
			_partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();

			_partnersPromoCodeLimitService = fixture.Build<PartnerPromoCodeLimitService>().Create();
		}

		[Fact]
		public void CreateNewLimit_LimitCreated_ReturnsNewLimit()
		{
			// Arrange
			var partner = PartnerFactory.CreateValidPartner();

			_partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
				.ReturnsAsync(partner);

			// Act
			var result = _partnersPromoCodeLimitService.CreateNewLimitPartner(partner, 2, new DateTime(2023, 05, 05));

			// Assert
			partner.PartnerLimits.Count.Should().Be(2);
		}

		[Fact]
		public void DeactivateLimitPartner_LimitCreated_ReturnsNumberIssuedPromoCodesIsZero()
		{
			// Arrange
			
			var partner = PartnerFactory.CreateValidPartner();

			_partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
				.ReturnsAsync(partner);

			var activeLimit = partner.PartnerLimits.FirstOrDefault(x =>
				!x.CancelDate.HasValue);

			// Act
			_partnersPromoCodeLimitService.DeactivateLimitPartner(partner, activeLimit);

			// Assert
			partner.NumberIssuedPromoCodes.Should().Be(0);
			activeLimit.CancelDate.Should().HaveValue();
		}
	}
}