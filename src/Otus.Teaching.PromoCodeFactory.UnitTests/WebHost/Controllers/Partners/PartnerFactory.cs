﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Collections.Generic;
using System;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
	public class PartnerFactory
	{
		public static Partner CreateValidPartner() => new Partner()
		{
			Id = Guid.Parse("0da65561-cf56-4942-bff2-22f50cf70d43"),
			Name = "Рыба твоей мечты",
			IsActive = true,
			NumberIssuedPromoCodes = 1,
			PartnerLimits = new List<PartnerPromoCodeLimit>()
			{
				new PartnerPromoCodeLimit()
				{
					Id = Guid.Parse("0691bb24-5fd9-4a52-a11c-34bb8bc9364e"),
					CreateDate = new DateTime(2023, 02, 22),
					EndDate = new DateTime(2024, 02, 22),
					Limit = 100
				}
			}
		};
	}
}