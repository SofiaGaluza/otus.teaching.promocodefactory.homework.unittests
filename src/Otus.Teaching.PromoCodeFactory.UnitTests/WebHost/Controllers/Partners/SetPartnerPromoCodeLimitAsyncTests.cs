﻿using System;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
	public class SetPartnerPromoCodeLimitAsyncTests
	{
		private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
		private readonly PartnersController _partnersController;

		public SetPartnerPromoCodeLimitAsyncTests()
		{
			var fixture = new Fixture().Customize(new AutoMoqCustomization());
			_partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
			_partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
		}

		[Fact]
		public async void SetPartnerPromoCodeLimitAsync_PartnerNotFound_ReturnsNotFound()
		{
			// Arrange
			var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
			Partner partner = null;

			_partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
				.ReturnsAsync(partner);

			var setPartnerPromoCodeLimitRequest = CreateSetPartnerPromoCodeLimitRequest(10);

			// Act
			var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, setPartnerPromoCodeLimitRequest);

			// Assert
			result.Should().BeAssignableTo<NotFoundResult>();
		}

		[Fact]
		public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
		{
			// Arrange
			var partner = PartnerFactory.CreateValidPartner();
			partner.IsActive = false;

			SetupRepository(partner);

			var setPartnerPromoCodeLimitRequest = CreateSetPartnerPromoCodeLimitRequest(10);

			// Act
			var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, setPartnerPromoCodeLimitRequest);

			// Assert
			result.Should().BeAssignableTo<BadRequestObjectResult>()
				.Which.Value.Should().Be("Данный партнер не активен");
		}

		[Fact]
		public async void SetPartnerPromoCodeLimitAsync_SetNumberIssuedPromoCodesIsNull_ReturnsOk()
		{
			// Arrange
			var partner = PartnerFactory.CreateValidPartner();

			SetupRepository(partner);

			var setPartnerPromoCodeLimitRequest = CreateSetPartnerPromoCodeLimitRequest(10);

			// Act
			var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, setPartnerPromoCodeLimitRequest);

			// Assert
			result.Should().BeAssignableTo<CreatedAtActionResult>();
			partner.NumberIssuedPromoCodes.Should().Be(0);
		}

		[Fact]
		public async void SetPartnerPromoCodeLimitAsync_SetCancelDateToLimit_ReturnsOk()
		{
			// Arrange
			var partner = PartnerFactory.CreateValidPartner();
			var prevLimit = partner.PartnerLimits.FirstOrDefault();

			SetupRepository(partner);

			var setPartnerPromoCodeLimitRequest = CreateSetPartnerPromoCodeLimitRequest(10);

			// Act
			var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, setPartnerPromoCodeLimitRequest);

			// Assert
			result.Should().BeAssignableTo<CreatedAtActionResult>();
			prevLimit.CancelDate.Should().NotBeNull();
		}

		[Fact]
		public async void SetPartnerPromoCodeLimitAsync_PartnerLimitHasPositiveValue_ReturnsBadRequest()
		{
			// Arrange
			var partner = PartnerFactory.CreateValidPartner();

			SetupRepository(partner);

			var setPartnerPromoCodeLimitRequest = CreateSetPartnerPromoCodeLimitRequest(0);

			// Act
			var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, setPartnerPromoCodeLimitRequest);

			// Assert
			result.Should().BeAssignableTo<BadRequestObjectResult>()
				.Which.Value.Should().Be("Лимит должен быть больше 0");
		}

		[Fact]
		public async void SetPartnerPromoCodeLimitAsync_AddLimit_ReturnsOk()
		{
			// Arrange
			var partner = PartnerFactory.CreateValidPartner();

			SetupRepository(partner);

			var setPartnerPromoCodeLimitRequest = CreateSetPartnerPromoCodeLimitRequest(10);

			// Act
			var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, setPartnerPromoCodeLimitRequest);

			// Assert
			result.Should().BeAssignableTo<CreatedAtActionResult>();
			partner.PartnerLimits.Count.Should().Be(2);
		}

		private static SetPartnerPromoCodeLimitRequest CreateSetPartnerPromoCodeLimitRequest(int limit)
		{
			return new Fixture()
				.Build<SetPartnerPromoCodeLimitRequest>()
				.With(e => e.Limit, limit)
				.Create();
		}

		private void SetupRepository(Partner partner)
		{
			if (partner != null)
				_partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
				.ReturnsAsync(partner);
		}
	}
}