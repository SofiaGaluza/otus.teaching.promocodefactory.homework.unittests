﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
	public interface IPartnerPromoCodeLimitService
	{
		void DeactivateLimitPartner(Partner partner, PartnerPromoCodeLimit activeLimit);
		PartnerPromoCodeLimit CreateNewLimitPartner(Partner partner, int limit, DateTime endDate);
	}
}