﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
	public class PartnerPromoCodeLimitService : IPartnerPromoCodeLimitService
	{
		public void DeactivateLimitPartner(Partner partner, PartnerPromoCodeLimit activeLimit)
		{
			if (activeLimit == null) return;

			//Если партнеру выставляется лимит, то мы 
			//должны обнулить количество промокодов, которые партнер выдал, если лимит закончился, 
			//то количество не обнуляется
			partner.NumberIssuedPromoCodes = 0;

			//При установке лимита нужно отключить предыдущий лимит
			activeLimit.CancelDate = DateTime.Now;
		}

		public PartnerPromoCodeLimit CreateNewLimitPartner(Partner partner, int limit, DateTime endDate)
		{
			var newLimit = new PartnerPromoCodeLimit()
			{
				Limit = limit,
				Partner = partner,
				PartnerId = partner.Id,
				CreateDate = DateTime.Now,
				EndDate = endDate
			};

			partner.PartnerLimits.Add(newLimit);

			return newLimit;
		}
	}
}